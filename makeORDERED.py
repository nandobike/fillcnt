import numpy as np
import random
import math


radius=6.0
half_box=15.383635

atoms_circle=16 #total number of atoms to place
atoms_length=13

atoms_total=atoms_circle*atoms_length
print("atoms_total=",atoms_total)

if (atoms_total < 1):
	print("Too few total atoms, exiting.")
	exit()


def distance(index1,index2): #trial is vector pos_try, index is the atom number in positions
	vect_dist = positions[index1]-positions[index2]
	#print vect_dist
	return math.sqrt(sum(vect_dist**2))



positions=np.zeros((atoms_total,3))

k=0

for i in range(0,atoms_circle): #i= where to put atom
	angle=2*np.pi*float(i)/atoms_circle
	position_x=radius*np.cos(angle)
	position_y=radius*np.sin(angle)
	for j in range(0,atoms_length): #i= where to put atom
		position_z=-half_box+float(j)/atoms_length*2*half_box
		positions[k]=[position_x,position_y,position_z]
		print("j=",j,"pos=",positions[k])
		k+=1
		#print(k)


print(positions[0])
print(positions[1])
print("axial distance= ",distance(0,1))
print(positions[atoms_length])
print("radial distance= ",distance(0,atoms_length))



#print("\nFinal positions:\n")#, positions
#print(positions)
			

print("z_size=",2*half_box)



# Create XYZ file
f= open("fillS.xyz","w+")
f.write("%d\n" % (atoms_total))
f.write("Comment\n")
for i in range(0,atoms_total):
     f.write("S\t%f\t%f\t%f\n" % (positions[i][0], positions[i][1], positions[i][2]))
f.close() 



# Create positions ready for LAMMPS
f= open("fillS.input","w+")
f.write("# S in CNT\n\n")
f.write("%d atoms\n" % (atoms_total))
f.write("1 atom types\n\n")
f.write("%f %f xlo xhi\n" % ( float(-half_box), float(half_box) ))
f.write("%f %f ylo yhi\n" % ( float(-half_box), float(half_box) ))
f.write("%f %f zlo zhi\n\n" % ( float(-half_box), float(half_box) ))
f.write("Masses\n\n")
f.write("1 32.065\n\n") #change this if not Sulfur
f.write("#charge: atom-ID atom-type q x y z\n")
f.write("Atoms\n\n")

for i in range(0,atoms_total):
     f.write("%d\t1\t0.0\t%f\t%f\t%f\n" % (i+1, positions[i][0], positions[i][1], positions[i][2]))
f.close() 

# Create positions ready to be pasted in MOPAC
f= open("fillS.mop","w+")
f.write(" AUX LARGE CHARGE=0 SINGLET  PM7 GRAPHF GNORM=0.0 LBFGS\n\n")
for i in range(0,atoms_total):
     f.write("S\t%f\t1\t%f\t1\t%f\t1\n" % (positions[i][0], positions[i][1], positions[i][2]))
f.write("Tv\t%f\t0\t%f\t0\t%f\t1\n" % (0.0, 0.0, 2*half_box))
f.close() 

