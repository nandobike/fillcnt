import numpy as np
import random
import math


radius=6.0
half_box=2.5

atoms_circle=11 #total number of atoms to place
n_loops=1
displacement_bond=1.0

atoms_total=atoms_circle*n_loops
print("atoms_total=",atoms_total)

if (atoms_total < 1):
	print("Too few total atoms, exiting.")
	exit()


def distance(index1,index2): #trial is vector pos_try, index is the atom number in positions
	vect_dist = positions[index1]-positions[index2]
	#print vect_dist
	return math.sqrt(sum(vect_dist**2))



positions=np.zeros((atoms_total,3))

k=0

for i in range(0,atoms_total): #i= where to put atom
	angle=2*np.pi*i/atoms_circle
	position_x=radius*np.cos(angle)
	position_y=radius*np.sin(angle)
	position_z=-half_box+float(i)/atoms_total*2*half_box
	position_z+=((i%2)-0.5)*displacement_bond
	positions[k]=[position_x,position_y,position_z]
	#print("j=",j,"pos=",positions[k])
	k+=1
	print (i,angle,position_x,position_y,position_z)






print(positions[0])
print(positions[1])
#print("axial distance= ",distance(0,1))
#print(positions[atoms_circle])
#print("radial distance= ",distance(0,atoms_length))



#print("\nFinal positions:\n")#, positions
#print(positions)
			

print("z_size= %f\n" % (2.0*half_box) )
print("bond distance= %f" % (distance(0,1)))



# Create XYZ file
f= open("fillS_HELIX.xyz","w+")
f.write("%d\n" % (atoms_total))
f.write("Comment\n")
for i in range(0,atoms_total):
     f.write("S\t%f\t%f\t%f\n" % (positions[i][0], positions[i][1], positions[i][2]))
f.close() 


# Create positions ready for Gaussian
f= open("fillS_HELIX.gjf","w+")
f.write("%nprocshared=6\n")
f.write("#p opt HSEh1PBE/6-31g(d,p)/Auto SCF=(NoVarAcc,NoIncFock,Tight) Int(Grid=Ultrafine)\n\n")
#f.write("IOp(5/13=1,5/33=1,5/181=10,5/184=186)\n\n")
f.write("%Commet\n\n")
f.write("0 1\n")
for i in range(0,atoms_total):
	f.write("S , %f , %f , %f\n" % (positions[i][0], positions[i][1], positions[i][2]))
f.write("TV , %f , %f , %f\n\n\n" % (0.0 , 0.0 , 2*half_box) )
f.close() 


# Create positions ready for LAMMPS
f= open("fillS_HELIX.input","w+")
f.write("# S in CNT\n\n")
f.write("%d atoms\n" % (atoms_total))
f.write("1 atom types\n\n")
f.write("%f %f xlo xhi\n" % ( float(-half_box), float(half_box) ))
f.write("%f %f ylo yhi\n" % ( float(-half_box), float(half_box) ))
f.write("%f %f zlo zhi\n\n" % ( float(-half_box), float(half_box) ))
f.write("Masses\n\n")
f.write("1 32.065\n\n") #change this if not Sulfur
f.write("#charge: atom-ID atom-type q x y z\n")
f.write("Atoms\n\n")
for i in range(0,atoms_total):
     f.write("%d\t1\t0.0\t%f\t%f\t%f\n" % (i+1, positions[i][0], positions[i][1], positions[i][2]))
f.close() 



# Create positions ready to be pasted in MOPAC
f= open("fillS_HELIX.mop","w+")
f.write(" AUX LARGE CHARGE=0 SINGLET  PM7 GRAPHF GNORM=0.0 LBFGS\n\n")
for i in range(0,atoms_total):
     f.write("S\t%f\t1\t%f\t1\t%f\t1\n" % (positions[i][0], positions[i][1], positions[i][2]))
f.write("Tv\t%f\t0\t%f\t0\t%f\t1\n" % (0.0, 0.0, 2*half_box))
f.close() 




