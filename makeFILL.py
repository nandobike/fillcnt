import numpy as np
import random
import math

limit_dist=2.5 #minimum distance to accept placement
max_radius=7.5
half_box=15.383635

atoms_total=250 #total number of atoms to place
if (atoms_total < 1):
	print("Too few total atoms, exiting.")
	exit()



positions=np.zeros((atoms_total,3))


def create_trial():
	radius=random.random()*max_radius
	angle=random.random()*2*math.pi
	depth=(random.random()-0.5)*(half_box-limit_dist/2)*2

	x=radius*math.cos(angle)
	y=radius*math.sin(angle)
	z=depth

	return np.array([x,y,z])

def distance(trial,index): #trial is vector pos_try, index is the atom number in positions
	vect_dist = positions[index]-trial
	#print vect_dist
	return math.sqrt(sum(vect_dist**2))


positions[0] = create_trial() #first particle does not need test and can be located immediately
atoms_already=1 #initialize with the first placed atom





#positions=np.random.rand(atoms_total,3)
#print positions
#for i in range(0,atoms_total):
#	print positions[i]
#exit()

overlap=True


for i in range(1,atoms_total): #i= where to put atom
	print("i=",i)

	while (overlap):

		pos_try = create_trial()
		#print "pos try=",pos_try
		for j in range(0,atoms_already): #j= where to scan
			#print "j=",j
			#print "dist=",distance(pos_try,j)
			if (distance(pos_try,j) < limit_dist):
				overlap = True
				#print "overlapping"
				break
			overlap=False
	
	positions[i]=pos_try
	#print positions
	atoms_already+=1
	overlap=True

print("\nFinal positions:\n")#, positions
print(positions)
			











f= open("fillS.dat","w+")


for i in range(0,atoms_total):
     f.write("%d\t1\t0.0\t%f\t%f\t%f\n" % (i+1, positions[i][0], positions[i][1], positions[i][2]))


f.close() 


